﻿using Core.Entities;
using Core.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class BasketRepository : IBasketRepository
    {
        //private readonly StackExchange.Redis.IDatabase _database;
        //public BasketRepository(IConnectionMultiplexer redis)
        //{
        //_database = redis.GetDatabase();
        //}

        private readonly IMemoryCache cache;
        public BasketRepository(IMemoryCache memoryCache)
        {
            cache = memoryCache;
        }

        public async Task<bool> DeleteBasketAsync(string basketId)
        {
            cache.Remove(basketId);
            return await Task.FromResult(true);

            //return await _database.KeyDeleteAsync(basketId);
        }

        public async Task<CustomerBasket> GetBasketAsync(string basketId)
        {
            cache.TryGetValue(basketId, out CustomerBasket basket);
            return await Task.FromResult(basket);

            //var data = await _database.StringGetAsync(basketId);
            //return data.IsNullOrEmpty ? null : JsonSerializer.Deserialize<CustomerBasket>(data);
        }

        public async Task<CustomerBasket> UpdateBasketAsync(CustomerBasket basket)
        {
            cache.Set(basket.Id, basket, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromDays(5)));
            return await Task.FromResult(basket);

            //var created = await _database.StringSetAsync(basket.Id, JsonSerializer.Serialize(basket), TimeSpan.FromDays(30));
            //if (!created) return null;
            //return await GetBasketAsync(basket.Id);
        }
    }
}

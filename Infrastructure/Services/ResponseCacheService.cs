﻿using Core.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class ResponseCacheService : IResponseCacheService
    {
        private readonly IMemoryCache cache;
        public ResponseCacheService(IMemoryCache memoryCache)
        {
            cache = memoryCache;
        }

        public Task CacheResponseAsync(string cacheKey, object response, TimeSpan timeToLive)
        {
            if (response == null) Task.Run(() => { return; });
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            var serialisedResponse = JsonSerializer.Serialize(response, options);
            var res = cache.Set(cacheKey, serialisedResponse, new MemoryCacheEntryOptions().SetAbsoluteExpiration(timeToLive));
            return Task.FromResult(res);
        }

        public Task<string> GetCachedResponse(string cacheKey)
        {
            cache.TryGetValue(cacheKey, out string result);
            return Task.FromResult(result);
        }
    }

    //public class ResponseCacheService : IResponseCacheService
    //{
    //    private readonly IDatabase _database;
    //    public ResponseCacheService(IConnectionMultiplexer redis)
    //    {
    //        _database = redis.GetDatabase();
    //    }

    //    public async Task CacheResponseAsync(string cacheKey, object response, TimeSpan timeToLive)
    //    {
    //        if (response == null) return;

    //        var options = new JsonSerializerOptions
    //        {
    //            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
    //        };

    //        var serialisedResponse = JsonSerializer.Serialize(response, options);

    //        await _database.StringSetAsync(cacheKey, serialisedResponse, timeToLive);
    //    }

    //    public async Task<string> GetCachedResponse(string cacheKey)
    //    {
    //        var cachedResponse = await _database.StringGetAsync(cacheKey);

    //        if (cachedResponse.IsNullOrEmpty) return null;

    //        return cachedResponse;
    //    }
    //}
}

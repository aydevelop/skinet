﻿
using Core.Interfaces;
using Infrastructure.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace _Skinet.Extensions
{
    public static class BasketServiceExtensions
    {
        public static IServiceCollection AddBasketServices(this IServiceCollection services, IConfiguration configuration)
        {
            string connection = configuration.GetConnectionString("Redis");
            services.AddMemoryCache();
            services.AddScoped<IBasketRepository, BasketRepository>();
            services.AddSingleton<IConnectionMultiplexer>(c =>
            {
                var configuration = ConfigurationOptions.Parse(connection, true);
                return ConnectionMultiplexer.Connect(configuration);
            });

            return services;
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;

namespace _Skinet.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseApiController : ControllerBase
    {

    }
}

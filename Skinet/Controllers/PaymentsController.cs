﻿using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace _Skinet.Controllers
{
    public class PaymentsController : BaseApiController
    {

        private readonly IPaymentService paymentService;
        private readonly ILogger<PaymentsController> logger;
        public PaymentsController(IPaymentService paymentService, ILogger<PaymentsController> logger)
        {
            this.logger = logger;
            this.paymentService = paymentService;
        }

        [Authorize]
        [HttpPost("{basketId}")]
        public async Task<ActionResult<CustomerBasket>> CreateOrUpdatePaymentIntent(string basketId)
        {
            var basket = await paymentService.CreateOrUpdatePaymentIntent(basketId);
            return basket;
        }
    }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-server-error',
  templateUrl: './server-error.component.html',
  styleUrls: ['./server-error.component.scss'],
})
export class ServerErrorComponent implements OnInit {
  error: any;

  constructor(private router: Router) {
    let state = this.router.getCurrentNavigation()?.extras?.state;
    this.error = state?.error?.Message;
  }

  ngOnInit(): void {}
}

import { Component, OnInit } from '@angular/core';
import { AccountService } from 'app/account/account.service';
import { BasketService } from 'app/basket/basket.service';
import { IBasket } from 'app/shared/models/basket';
import { IUser } from 'app/shared/models/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit {
  basket$: Observable<IBasket>;
  currentUser$: Observable<IUser>;

  constructor(
    private basketService: BasketService,
    private accountService: AccountService
  ) {}

  ngOnInit(): void {
    this.basket$ = this.basketService.basket$;
    this.currentUser$ = this.accountService.currentUser$;

    // this.basketService.basket$.subscribe(
    //   (item) => {
    //     //console.log('basket item... ' + JSON.stringify(item));
    //   },
    //   (error) => {
    //     console.log(error);
    //   }
    // );
  }

  logout() {
    this.accountService.logout();
  }
}
